<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Ares\Controller\AresDataByIcoController;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('get_ares_data', '/api/app/public/ares')
        ->controller(AresDataByIcoController::class)
        ->methods(['GET']);
};
