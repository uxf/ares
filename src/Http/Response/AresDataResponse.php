<?php

declare(strict_types=1);

namespace UXF\Ares\Http\Response;

use UXF\Ares\Dto\AresRzpDataDto;
use UXF\Ares\Dto\AresStandardDataDto;
use UXF\Ares\Http\AresSubjectType;

final readonly class AresDataResponse
{
    public function __construct(
        public string $ico,
        public string $name,
        public ?int $smartAddressId,
        public bool $vatPayer,
        public ?string $vatNumber,
        public ?string $firstname,
        public ?string $surname,
        public ?bool $person,
        public ?bool $isForeign,
        public ?AresSubjectType $subjectType,
    ) {
    }

    public static function createFromDto(
        AresStandardDataDto $aresDto,
        ?AresRzpDataDto $aresRzpDataDto,
    ): self {
        return new self(
            $aresDto->ico,
            $aresDto->name,
            $aresDto->smartAddressId,
            $aresDto->vatPayer,
            $aresDto->dic,
            $aresRzpDataDto?->firstname,
            $aresRzpDataDto?->surname,
            $aresRzpDataDto?->person,
            $aresRzpDataDto?->isForeign,
            $aresRzpDataDto?->subjectType,
        );
    }
}
