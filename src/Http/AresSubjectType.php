<?php

declare(strict_types=1);

namespace UXF\Ares\Http;

use UXF\Ares\Exception\AresSubjectTypeException;
use UXF\Core\Attribute\AnyTrait;
use UXF\Core\Attribute\Label;
use UXF\Core\Attribute\LabelTrait;

/**
 * zdroj číselníku: POST https://ares.gov.cz/ekonomicke-subjekty-v-be/rest/ciselniky-nazevniky/vyhledat
 * {
     "start": 0,
     "pocet": 10,
     "razeni": [
     "kodCiselniku"
     ],
     "zdrojCiselniku": "rzp",
     "kodCiselniku": "TypSubjektu"
     }
 */
enum AresSubjectType: string
{
    use LabelTrait;
    use AnyTrait;

    #[Label('Fyzická osoba tuzemská')]
    case LOCAL_PERSON = 'LOCAL_PERSON';

    #[Label('Fyzická osoba s organizační složkou')]
    case ORGANISATIONAL_UNIT = 'ORGANISATIONAL_UNIT';

    #[Label('Právnická osoba tuzemská')]
    case LOCAL_COMPANY = 'LOCAL_COMPANY';

    #[Label('Zahraniční právnická osoba v roli statutárního orgánu')]
    case STATUTORY_BODY = 'STATUTORY_BODY';

    #[Label('Zahraniční právnická osoba')]
    case FOREIGN_COMPANY = 'FOREIGN_COMPANY';

    #[Label('Zahraniční právnická osoba')]
    case FOREIGN_PERSON = 'FOREIGN_PERSON';

    public static function fromAresCode(string $aresCode): self
    {
        return match ($aresCode) {
            'F' => self::LOCAL_PERSON,
            'O' => self::ORGANISATIONAL_UNIT,
            'P' => self::LOCAL_COMPANY,
            'S' => self::STATUTORY_BODY,
            'X' => self::FOREIGN_COMPANY,
            'Z' => self::FOREIGN_PERSON,
            default => throw new AresSubjectTypeException($aresCode),
        };
    }
}
