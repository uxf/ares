<?php

declare(strict_types=1);

namespace UXF\Ares\Http\Request;

final readonly class AresRequestQuery
{
    public function __construct(
        public string $ico,
    ) {
    }
}
