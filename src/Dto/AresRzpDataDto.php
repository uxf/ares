<?php

declare(strict_types=1);

namespace UXF\Ares\Dto;

use UXF\Ares\Exception\AresParsingException;
use UXF\Ares\Http\AresSubjectType;

final readonly class AresRzpDataDto
{
    public function __construct(
        public string $ico,
        public ?string $firstname,
        public ?string $surname,
        public bool $person,
        public bool $isForeign,
        public AresSubjectType $subjectType,
    ) {
    }

    /**
     * @param mixed[] $aresArray
     */
    public static function createFromArrayV2(array $aresArray): self
    {
        $record = $aresArray['zaznamy'][0];

        $persons = $record['angazovaneOsoby'] ?? [];
        $person = $persons[0] ?? null;
        $ico = $record['ico'] ?? null;
        $firstname = $person['jmeno'] ?? null;
        $lastname = $person['prijmeni'] ?? null;
        $subjectType = AresSubjectType::fromAresCode($record['typSubjektu']);
        $isPerson = $subjectType->any(AresSubjectType::LOCAL_PERSON, AresSubjectType::FOREIGN_PERSON);
        $isForeign = $subjectType->any(AresSubjectType::FOREIGN_COMPANY, AresSubjectType::FOREIGN_PERSON, AresSubjectType::STATUTORY_BODY);



        if (count($persons) !== 1) {
            return new self(
                $ico,
                null,
                null,
                $isPerson,
                $isForeign,
                $subjectType,
            );
        }

        if (
            $firstname === null
            || $lastname === null
        ) {
            throw new AresParsingException($record);
        }

        return new self(
            $ico,
            $firstname,
            $lastname,
            $isPerson,
            $isForeign,
            $subjectType,
        );
    }
}
