<?php

declare(strict_types=1);

namespace UXF\Ares\Dto;

use Throwable;
use UXF\Ares\Exception\AresParsingException;

final readonly class AresStandardDataDto
{
    public function __construct(
        public string $ico,
        public string $name,
        public ?int $smartAddressId,
        public bool $vatPayer,
        public ?string $dic,
    ) {
    }

    /**
     * @param mixed[] $aresArray
     */
    public static function createFromArrayV2(array $aresArray): self
    {
        try {
            $ico = $aresArray['ico'] ?? null;
            $dic = $aresArray['dic'] ?? null;
            $name = $aresArray['obchodniJmeno'] ?? null;
            $smartAddressId = $aresArray['sidlo']['kodAdresnihoMista'] ?? null; // optional
            $vatPayer = $aresArray['seznamRegistraci']['stavZdrojeDph'] ?? null;

            if (!isset($ico, $name, $vatPayer)) {
                throw new AresParsingException($aresArray);
            }

            return new self(
                $ico,
                $name,
                $smartAddressId !== null ? (int) $smartAddressId : null,
                $vatPayer === 'AKTIVNI',
                $dic,
            );
        } catch (Throwable $e) {
            throw new AresParsingException($aresArray);
        }
    }
}
