<?php

declare(strict_types=1);

namespace UXF\Ares\Controller;

use UXF\Ares\Http\Request\AresRequestQuery;
use UXF\Ares\Http\Response\AresDataResponse;
use UXF\Ares\Service\AresDataDownloader;
use UXF\Core\Attribute\FromQuery;

final readonly class AresDataByIcoController
{
    public function __construct(
        private AresDataDownloader $aresDataDownloader,
    ) {
    }

    public function __invoke(#[FromQuery] AresRequestQuery $body): AresDataResponse
    {
        return $this->aresDataDownloader->downloadAresData($body->ico);
    }
}
