<?php

declare(strict_types=1);

namespace UXF\Ares\Service;

use UXF\Ares\Client\AresClientV2;
use UXF\Ares\Dto\AresRzpDataDto;
use UXF\Ares\Dto\AresStandardDataDto;
use UXF\Ares\Exception\AresDownloadingException;
use UXF\Ares\Exception\AresParsingException;
use UXF\Ares\Http\Response\AresDataResponse;

/**
 * https://ares.gov.cz/swagger-ui/#/ciselniky-nazevniky/vyhledejCiselnik
 *
 */
final readonly class HttpAresDataDownloader implements AresDataDownloader
{
    public function __construct(
        private AresClientV2 $aresClientV2,
    ) {
    }

    /**
     * @throws AresParsingException
     * @throws AresDownloadingException
     */
    public function downloadAresData(string $ico): AresDataResponse
    {
        $standartData = $this->downloadStandardAresData($ico);
        try {
            $rzpData = $this->downloadRzpAresData($ico);
        } catch (AresDownloadingException) {
            $rzpData = null;
        }

        return AresDataResponse::createFromDto(
            $standartData,
            $rzpData,
        );
    }

    /**
     * @throws AresParsingException
     * @throws AresDownloadingException
     */
    public function downloadStandardAresData(string $ico): AresStandardDataDto
    {
        return $this->aresClientV2->getStandardAresData($ico);
    }

    /**
     * @throws AresParsingException
     * @throws AresDownloadingException
     */
    public function downloadRzpAresData(string $ico): AresRzpDataDto
    {
        return $this->aresClientV2->getRzpAresData($ico);
    }
}
