<?php

declare(strict_types=1);

namespace UXF\AresTests;

use LogicException;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use function Safe\file_get_contents;

class MockClient implements ClientInterface
{
    public function __construct()
    {
    }

    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        $url = $request->getUri()->__toString();

        $matches = [];
        if (preg_match('/.*\/(ekonomicke-subjekt.*\/.*)/', $url, $matches, PREG_OFFSET_CAPTURE) !== 1) {
            throw new LogicException();
        }

        $filename = str_replace('/', '_', $matches[1][0]);

        // ico 111 is fake - for testing purposes
        if (str_ends_with($filename, '_111')) {
            return new Response(400);
        }

        return new Response(200, [], $this->getBody($filename, $url));
    }

    private function getBody(string $filename, string $url): StreamInterface
    {
        $filename = __DIR__ . '/Integration/Expected/' . $filename . '.json';
        if (file_exists($filename)) {
            $body = Stream::create(file_get_contents($filename));
            $body->rewind();
        } else {
            $text = file_get_contents($url);
            file_put_contents($filename, json_encode(json_decode($text), JSON_PRETTY_PRINT));
            $body = Stream::create($text);
        }

        return $body;
    }
}
